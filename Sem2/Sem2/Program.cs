﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Example
{
    public static class Program
    {
        public static void Main()
        {
            var array1 = new int[] { 4, 7, 1, 5, 9, 3 };
            PatienceSort(array1);

            var array2 = new int[] { 4, 7, 1, 5, 9, 3 };
            PatienceSort(array2);
        }

        //merge sort
        public static void MergeSort(int[] array)
        {
            if (array.Length < 2)
                return;

            var middle = array.Length / 2;
            var left = new int[middle];
            var right = new int[array.Length - middle];

            for (int i = 0; i < middle; i++)
                left[i] = array[i];

            var k = 0;
            for (int i = middle; i < array.Length; i++)
            {
                right[k] = array[i];
                k++;
            }

            MergeSort(left);
            MergeSort(right);
            Merge(left, right, array);
        }
        public static void Merge(int[] left, int[] right, int[] array)
        {
            int i = 0, j = 0, k = 0;
            while (i < left.Length && j < right.Length)
            {
                if (left[i] <= right[j])
                {
                    array[k] = left[i];
                    i++;
                }
                else
                {
                    array[k] = right[j];
                    j++;
                }
                k++;
            }
            while (i < left.Length)
            {
                array[k] = left[i];
                i++;
            }
            while (j < right.Length)
            {
                array[k] = right[j];
                j++;
            }
            k++;
        }

        //patience sort

        public static void PatienceSort(int[] array)
        {
            var piles = array.ArrayToPiles();
            array = array.PushBackFromPilesToArray(piles).ToArray();
        }

        public static List<Stack<int>> ArrayToPiles(this int[] array)
        {
            var resultPiles = new List<Stack<int>>();

            foreach (var number in array)
            {
                var minPileNumber = -1;
                var minPilePeek = int.MaxValue;

                for (int i = 0; i < resultPiles.Count; i++)
                {
                    if (resultPiles[i].Peek() >= number &&
                        resultPiles[i].Peek() <= minPilePeek)
                    {
                        minPileNumber = i;
                        minPilePeek = resultPiles[minPileNumber].Peek();
                    }
                }

                if (minPileNumber != -1)
                    resultPiles[minPileNumber].Push(number);
                else
                {
                    var stack = new Stack<int>();
                    stack.Push(number);
                    resultPiles.Add(stack);
                }
            }
            return resultPiles;
        }

        public static IEnumerable<int> PushBackFromPilesToArray(this int[] array, List<Stack<int>> piles)
        {
            var minPileNumber = -1;
            var minPilePeek = int.MaxValue;

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < piles.Count; j++)
                {
                    if (piles[j].Count() == 0)
                        continue;
                    if (piles[j].Peek() <= minPilePeek)
                    {
                        minPileNumber = j;
                        minPilePeek = piles[minPileNumber].Peek();
                    }
                }
                yield return piles[minPileNumber].Pop();
                minPileNumber = -1;
                minPilePeek = int.MaxValue;
            }
        }
    }
}